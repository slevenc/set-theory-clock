package com.paic.arch.interviews.lamps;

import com.paic.arch.interviews.TimeLampsRow;

/**
 * Created by slevenc on 2018/2/8.
 */
public abstract class TimeBeanLampRow implements TimeLampsRow {


    @Override
    public String lampOutput(String aTime) {
        return acceptTimeBean(TimeBean.parse(aTime));
    }

    public abstract String acceptTimeBean(TimeBean timeBean);
}
