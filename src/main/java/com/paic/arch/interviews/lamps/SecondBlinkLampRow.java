package com.paic.arch.interviews.lamps;

/**
 * There is a yellow lamp that blinks on/off every two seconds.
 * Created by slevenc on 2018/2/8.
 */
public class SecondBlinkLampRow extends TimeBeanLampRow {

    @Override
    public String acceptTimeBean(TimeBean timeBean) {
        int sec = timeBean.get(TimeBean.SECOND);
        if (sec % 2 == 0) {
            return "Y";
        }
        return "O";
    }
}
