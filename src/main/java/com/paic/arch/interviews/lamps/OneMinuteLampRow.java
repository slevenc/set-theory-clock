package com.paic.arch.interviews.lamps;

/**
 * In the last row with 4 lamps every lamp represents 1 minute.
 * Created by slevenc on 2018/2/8.
 */
public class OneMinuteLampRow extends TimeBeanLampRow {

    @Override
    public String acceptTimeBean(TimeBean timeBean) {
        int minute = timeBean.get(TimeBean.MINUTE);
        StringBuilder sb = new StringBuilder();
        int lightCount = minute % 5;
        for (int i = 0; i < 4; i++) {
            if (lightCount-- > 0) {
                sb.append("Y");
            } else {
                sb.append("O");
            }
        }
        return sb.toString();
    }
}
