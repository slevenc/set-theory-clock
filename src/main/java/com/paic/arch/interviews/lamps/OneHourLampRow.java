package com.paic.arch.interviews.lamps;

/**
 * In the lower row of red lamps every lamp represents 1 hour.
 * Created by slevenc on 2018/2/8.
 */
public class OneHourLampRow extends TimeBeanLampRow {

    @Override
    public String acceptTimeBean(TimeBean timeBean) {
        int hours = timeBean.get(TimeBean.HOUR);
        StringBuilder sb = new StringBuilder();
        int lightCount = hours % 5;
        for (int i = 0; i < 4; i++) {
            if (lightCount-- > 0) {
                sb.append("R");
            } else {
                sb.append("O");
            }
        }
        return sb.toString();
    }
}
