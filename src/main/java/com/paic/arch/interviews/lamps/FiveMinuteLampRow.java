package com.paic.arch.interviews.lamps;

/**
 * The first of these rows has 11 lamps,
 * In the first row every lamp represents 5 minutes.
 * In this first row the 3rd, 6th and 9th lamp are red and indicate the first quarter, half and last quarter of an hour.
 * Created by slevenc on 2018/2/8.
 */
public class FiveMinuteLampRow extends TimeBeanLampRow {

    @Override
    public String acceptTimeBean(TimeBean timeBean) {
        int minute = timeBean.get(TimeBean.MINUTE);
        StringBuilder sb = new StringBuilder();
        int lightCount = minute / 5;
        for (int i = 0; i < 11; i++) {
            if (lightCount-- > 0) {
                sb.append(light(i));
            } else {
                sb.append("O");
            }
        }
        return sb.toString();
    }

    private String light(int index) {
        if ((index + 1) % 3 == 0) {
            return "R";
        }
        return "Y";
    }
}
