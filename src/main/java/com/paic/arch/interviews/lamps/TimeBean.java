package com.paic.arch.interviews.lamps;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * bean with parsed  HH:MM:SS
 * Created by slevenc on 2018/2/8.
 */
public class TimeBean {


    public static final int HOUR = 0;
    public static final int MINUTE = 1;
    public static final int SECOND = 2;
    protected int fields[];

    private TimeBean() {

    }

    public static TimeBean parse(String timeStr) {
        if (timeStr == null || timeStr.trim().isEmpty()) {
            throw new IllegalArgumentException();
        }
        Matcher digisFinder = Pattern.compile("\\d+").matcher(timeStr);
        int[] fields = new int[3];
        try {
            for (int i = 0; i < 3; i++) {
                if (!digisFinder.find()) {
                    throw new IllegalArgumentException();
                }
                int val = Integer.valueOf(digisFinder.group(), 10);
                fields[i] = val;
            }
        } catch (Exception ex) {
            throw new IllegalArgumentException();
        }
        TimeBean bean = new TimeBean();
        bean.fields = fields;
        return bean;

    }

    public int get(int field) {
        return fields[field];
    }
}
