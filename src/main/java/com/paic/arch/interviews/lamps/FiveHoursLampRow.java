package com.paic.arch.interviews.lamps;

/**
 * There are 4 red lamps. Every lamp represents 5 hours.
 * Created by slevenc on 2018/2/8.
 */
public class FiveHoursLampRow extends TimeBeanLampRow {

    @Override
    public String acceptTimeBean(TimeBean timeBean) {
        int hours = timeBean.get(TimeBean.HOUR);
        StringBuilder sb = new StringBuilder();
        int lightCount = hours / 5;
        for (int i = 0; i < 4; i++) {
            if (lightCount-- > 0) {
                sb.append("R");
            } else {
                sb.append("O");
            }
        }
        return sb.toString();
    }
}
