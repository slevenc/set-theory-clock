package com.paic.arch.interviews;


import com.paic.arch.interviews.lamps.*;

/**
 * Created by slevenc on 2018/2/8.
 */
public class TimeConverterImpl implements TimeConverter {

    protected TimeLampsRow[] timeLampsRows;


    public TimeConverterImpl() {
        this.timeLampsRows = new TimeLampsRow[]{
                new SecondBlinkLampRow(),
                new FiveHoursLampRow(),
                new OneHourLampRow(),
                new FiveMinuteLampRow(),
                new OneMinuteLampRow()
        };
    }

    @Override
    public String convertTime(String aTime) {
        StringBuilder sb = new StringBuilder();
        for (TimeLampsRow lampRow : timeLampsRows) {
            sb.append(lampRow.lampOutput(aTime));
            sb.append("\n");
        }
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }
}
