package com.paic.arch.interviews;

/**
 * Created by slevenc on 2018/2/8.
 */
public interface TimeLampsRow {

    String lampOutput(String aTime);
}
